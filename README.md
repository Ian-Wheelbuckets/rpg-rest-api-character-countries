### [AVERTISSMENT] Problèmes de droits

En cas d'échec de l'étape de build dans le pipeline avec une erreur de type "/bin/sh: eval: line 96: ./mvnw: Permission denied", modifier les droits en local et pousser la modification via la commande:
> git update-index --chmod=+x mvnw