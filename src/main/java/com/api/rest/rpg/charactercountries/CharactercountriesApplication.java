package com.api.rest.rpg.charactercountries;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharactercountriesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CharactercountriesApplication.class, args);
	}

}
