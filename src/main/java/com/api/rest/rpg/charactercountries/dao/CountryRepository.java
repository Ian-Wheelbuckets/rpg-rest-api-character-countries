package com.api.rest.rpg.charactercountries.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.rest.rpg.charactercountries.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {
	public Optional<Country> findByAlpha2(String alpha2);
}
