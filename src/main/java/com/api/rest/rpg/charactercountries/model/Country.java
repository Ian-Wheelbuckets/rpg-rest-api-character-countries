package com.api.rest.rpg.charactercountries.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "countries")
@EntityListeners(AuditingEntityListener.class)
public class Country implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "code", nullable = false)
	private Integer code;
	
	@Column(name = "alpha_2", nullable = false)
	private String alpha2;
	
	@Column(name = "alpha_3", nullable = false)
	private String alpha3;
	
	@Column(name = "name_fr", nullable = false)
	private String nameFr;
	
	@Column(name = "name_en", nullable = false)
	private String nameEn;

	public Country() {
	}

	public Country(Integer code, String alpha2, String alpha3, String nameFr, String nameEn) {
		this.code = code;
		this.alpha2 = alpha2;
		this.alpha3 = alpha3;
		this.nameFr = nameFr;
		this.nameEn = nameEn;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @return the alpha2
	 */
	public String getAlpha2() {
		return alpha2;
	}

	/**
	 * @param alpha2 the alpha2 to set
	 */
	public void setAlpha2(String alpha2) {
		this.alpha2 = alpha2;
	}

	/**
	 * @return the alpha3
	 */
	public String getAlpha3() {
		return alpha3;
	}

	/**
	 * @param alpha3 the alpha3 to set
	 */
	public void setAlpha3(String alpha3) {
		this.alpha3 = alpha3;
	}

	/**
	 * @return the nameFr
	 */
	public String getNameFr() {
		return nameFr;
	}

	/**
	 * @param nameFr the nameFr to set
	 */
	public void setNameFr(String nameFr) {
		this.nameFr = nameFr;
	}

	/**
	 * @return the nameEn
	 */
	public String getNameEn() {
		return nameEn;
	}

	/**
	 * @param nameEn the nameEn to set
	 */
	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Country [code=" + code + ", alpha2=" + alpha2 + ", alpha3=" + alpha3 + ", nameFr=" + nameFr
				+ ", nameEn=" + nameEn + "]";
	}
}
