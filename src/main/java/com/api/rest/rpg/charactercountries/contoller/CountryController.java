package com.api.rest.rpg.charactercountries.contoller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.rest.rpg.charactercountries.dao.CountryRepository;
import com.api.rest.rpg.charactercountries.exception.UnreachableCountryException;
import com.api.rest.rpg.charactercountries.model.Country;

@RestController
@RequestMapping("/api")
public class CountryController {
	
	@Autowired
	private CountryRepository countryRep;
	
	@RequestMapping(value="/countries", method=RequestMethod.GET)
	public List<Country> getAllCountries() {
		List<Country> cs = countryRep.findAll();
		
		if(cs==null)
			throw new UnreachableCountryException("The serveur is unable to reach the list of last names.");
		
		return cs;
	}
	
	@RequestMapping(value="/countries/id/{id}", method=RequestMethod.GET)
	public Optional<Country> getCountryById(@PathVariable Integer id) {
		Optional<Country> c = countryRep.findById(id);
		
		if(!c.isPresent())
			throw new UnreachableCountryException("The serveur is unable to reach the  last name.");
		
		return c;
	}
	
	@RequestMapping(value="/countries/alpha2/{alpha2}", method=RequestMethod.GET)
	public Optional<Country> getCountryByAlpha2(@PathVariable String alpha2) {
		Optional<Country> c = countryRep.findByAlpha2(alpha2);
		
		if(!c.isPresent())
			throw new UnreachableCountryException("The serveur is unable to reach the  last name.");
		
		return c;
	}
}
