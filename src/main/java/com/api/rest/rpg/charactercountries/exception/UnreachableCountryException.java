package com.api.rest.rpg.charactercountries.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UnreachableCountryException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnreachableCountryException(String s) {
		super(s);
	}
	
}
